import React, { Component } from "react";
import styled from "styled-components";
import { Form, Button, Input, Label, Step } from "semantic-ui-react";

import OSGridRef from "geodesy/osgridref.js";

const Wrapper = styled.div`
  padding: 20px;
`;

const FormWrapper = styled.div`
  width: 200px;
`;

class App extends Component {
  state = {
    output: "https://maps.google.com/?q=52.433385190011016,-1.8868912693301834",
    xValue: 407787,
    yValue: 281718
  };

  handleClick = () => {
    const osRef = new OSGridRef(
      parseFloat(this.state.xValue),
      parseFloat(this.state.yValue)
    );
    const pt = OSGridRef.osGridToLatLon(osRef);
    // const googleMapLink = `https://maps.google.com/?q=${pt.lat},${pt.lon}`;

    const googleMapLink =
      `https://www.google.com/maps/dir/?api=1&origin=my+location` +
      `&destination=${pt.lat},${pt.lon}&travelmode=driving&dir_action=navigate`;

    this.setState({ output: googleMapLink });
  };

  handleXChange = e => {
    this.setState({ xValue: e.target.value });
  };

  handleYChange = e => {
    this.setState({ yValue: e.target.value });
  };

  copyToClipBoard = text => {
    const textField = document.createElement("textarea");
    textField.innerText = text;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand("copy");
    textField.remove();
  };

  render() {
    return (
      <Wrapper>
        <h2>Coordinate Conversion</h2>
        <FormWrapper>
          <Form>
            <Form.Field>
              <Label>x</Label>
              <Input
                type={"number"}
                value={this.state.xValue}
                onChange={this.handleXChange}
              />
            </Form.Field>
            <Form.Field>
              <Label>y</Label>
              <Input
                type={"number"}
                value={this.state.yValue}
                onChange={this.handleYChange}
              />
            </Form.Field>
            <div>
              <Button type="button" onClick={this.handleClick}>
                Calculate
              </Button>
            </div>
          </Form>
        </FormWrapper>
        <Step.Group>
          <Step>{this.state.output}</Step>
        </Step.Group>
        <Button
          onClick={e => {
            console.log("CLICKITY");
            this.copyToClipBoard(this.state.output);
          }}
        >
          Copy to clipboard
        </Button>
      </Wrapper>
    );
  }
}

export default App;
