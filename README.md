# Coordinate Conversion App

## Description

This website converts Ordnance Survey coordinates Eastings and Northings to GPS 
coordinate directions in Google maps.

## And now for the Science Bit

The app was created using [create-react-app](https://github.com/facebook/create-react-app) and produces a build directory that requires a web server to host it.

It uses the geodesy library to perform the conversions. 
This library can be found here: https://www.npmjs.com/package/geodesy

 